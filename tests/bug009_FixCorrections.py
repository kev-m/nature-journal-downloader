# https://gitlab.com/kev-m/nature-journal-downloader/-/issues/9
#
# When Nature issues a clarification, correction or an addendum to an existing
# article, it is added to the end of the article's PDF.
#
# Sometimes that extra page is used instead of the actual next article page,
# resulting in an output PDF that is missing the next page.
#
# See Volume 578, Issue 7796, physical page 3 (journal page 490).
# At present, this clarification page 490 replaces the actual page 490.

import unittest

from collections import OrderedDict

# Reforming PDF
import nature_dl.parse_page_info as parse_page_info


class Bug009_FixCorrections(unittest.TestCase):

    def get_raw_dict(self):

        raw_dict = OrderedDict([('pdf7796/d41586-020-00532-4.pdf', [489, 490]),
                                ('pdf7796/d41586-020-00530-6.pdf', [489, 490]),
                                ('pdf7796/d41586-020-00531-5.pdf', [490]),
                                ('pdf7796/d41586-020-00505-7.pdf', [491]),
                                ('pdf7796/d41586-020-00495-6.pdf', [492]),
                                ('pdf7796/d41586-020-00493-8.pdf', [492]),
                                ('pdf7796/d41586-020-00456-z.pdf', [492]),
                                ('pdf7796/d41586-020-00427-4.pdf', [492]),
                                ('pdf7796/d41586-020-00494-7.pdf', [493]),
                                ('pdf7796/d41586-020-00483-w.pdf', [493]),
                                ('pdf7796/d41586-020-00473-y.pdf', [493]),
                                ('pdf7796/d41586-020-00506-6.pdf', [494, 495]),
                                ('pdf7796/d41586-020-00465-y.pdf', [497, 498]),
                                ('pdf7796/d41586-020-00478-7.pdf', [499]),
                                ('pdf7796/d41586-020-00497-4.pdf', [500]),
                                ('pdf7796/d41586-020-00489-4.pdf', [501]),
                                ('pdf7796/d41586-020-00507-5.pdf', [502, 503, 504]),
                                ('pdf7796/d41586-020-00508-4.pdf', [505, 506, 507]),
                                ('pdf7796/d41586-020-00509-3.pdf', [509, 510]),
                                ('pdf7796/d41586-020-00510-w.pdf', [510, 511]),
                                ('pdf7796/d41586-020-00500-y.pdf', [512, 513, 514]),
                                ('pdf7796/d41586-020-00517-3.pdf', [515]),
                                ('pdf7796/d41586-020-00241-y.pdf', [515]),
                                ('pdf7796/d41586-020-00522-6.pdf', [515]),
                                ('pdf7796/d41586-020-00523-5.pdf', [515]),
                                ('pdf7796/d41586-020-00420-x.pdf', [631, 632]),
                                ('pdf7796/d41586-020-00374-0.pdf', [632]),
                                ('pdf7796/d41586-020-00511-9.pdf', [633, 634, 635]),
                                ('pdf7796/d41586-020-00512-8.pdf', [638]),
                                ('pdf7796/d41586-020-00314-y.pdf', [517, 518]),
                                ('pdf7796/d41586-020-00403-y.pdf', [518, 519]),
                                ('pdf7796/d41586-020-00244-9.pdf', [520, 521]),
                                ('pdf7796/d41586-020-00468-9.pdf', [521, 522]),
                                ('pdf7796/d41586-020-00479-6.pdf', [522, 523, 524]),
                                ('pdf7796/d41586-020-00469-8.pdf', [524, 525])])

        return raw_dict

    def get_sorted_dict(self):
        sorted_dict = OrderedDict([('pdf7796/d41586-020-00532-4.pdf', [489, 490]),
                                   ('pdf7796/d41586-020-00530-6.pdf', [489, 490]),
                                   ('pdf7796/d41586-020-00531-5.pdf', [490]),
                                   ('pdf7796/d41586-020-00505-7.pdf', [491]),
                                   ('pdf7796/d41586-020-00495-6.pdf', [492]),
                                   ('pdf7796/d41586-020-00493-8.pdf', [492]),
                                   ('pdf7796/d41586-020-00456-z.pdf', [492]),
                                   ('pdf7796/d41586-020-00427-4.pdf', [492]),
                                   ('pdf7796/d41586-020-00494-7.pdf', [493]),
                                   ('pdf7796/d41586-020-00483-w.pdf', [493]),
                                   ('pdf7796/d41586-020-00473-y.pdf', [493]),
                                   ('pdf7796/d41586-020-00506-6.pdf', [494, 495]),
                                   ('pdf7796/d41586-020-00465-y.pdf', [497, 498]),
                                   ('pdf7796/d41586-020-00478-7.pdf', [499]),
                                   ('pdf7796/d41586-020-00497-4.pdf', [500]),
                                   ('pdf7796/d41586-020-00489-4.pdf', [501]),
                                   ('pdf7796/d41586-020-00507-5.pdf', [502, 503, 504]),
                                   ('pdf7796/d41586-020-00508-4.pdf', [505, 506, 507]),
                                   ('pdf7796/d41586-020-00509-3.pdf', [509, 510]),
                                   ('pdf7796/d41586-020-00510-w.pdf', [510, 511]),
                                   ('pdf7796/d41586-020-00500-y.pdf', [512, 513, 514]),
                                   ('pdf7796/d41586-020-00517-3.pdf', [515]),
                                   ('pdf7796/d41586-020-00241-y.pdf', [515]),
                                   ('pdf7796/d41586-020-00522-6.pdf', [515]),
                                   ('pdf7796/d41586-020-00523-5.pdf', [515]),
                                   ('pdf7796/d41586-020-00314-y.pdf', [517, 518]),
                                   ('pdf7796/d41586-020-00403-y.pdf', [518, 519]),
                                   ('pdf7796/d41586-020-00244-9.pdf', [520, 521]),
                                   ('pdf7796/d41586-020-00468-9.pdf', [521, 522]),
                                   ('pdf7796/d41586-020-00479-6.pdf', [522, 523, 524]),
                                   ('pdf7796/d41586-020-00469-8.pdf', [524, 525]),
                                   ('pdf7796/d41586-020-00420-x.pdf', [631, 632]),
                                   ('pdf7796/d41586-020-00374-0.pdf', [632]),
                                   ('pdf7796/d41586-020-00511-9.pdf', [633, 634, 635]),
                                   ('pdf7796/d41586-020-00512-8.pdf', [638])])

        return sorted_dict

    def get_expected_data(self):
        file_pages_dictionary = self.get_sorted_dict()

        file_indices_dictionary, file_pages_dictionary = parse_page_info.extract_sequential_pages(
            file_pages_dictionary)

        expected_page1 = {}
        expected_page1['file'] = 'pdf7796/d41586-020-00532-4.pdf'
        expected_page1['pages'] = [489]
        expected_page1['indices'] = [0]

        expected_page2 = {}
        expected_page2['file'] = 'pdf7796/d41586-020-00531-5.pdf'
        expected_page2['pages'] = [490]
        expected_page2['indices'] = [0]

        return file_indices_dictionary, file_pages_dictionary, expected_page1, expected_page2

# Page: 0, Checking pdf7796/d41586-020-00532-4.pdf, with pages: [489, 490]
# Found page: 489, looking for page: 490, index: 0
# Found page: 490, looking for page: 491, index: 1
# Compiling file: pdf7796/d41586-020-00532-4.pdf, indices: [0, 1], pages: [489, 490]
# Found the last page of the previous article in the current article
# Dropping page 490 from file pdf7796/d41586-020-00532-4.pdf
# Page: 490, Checking pdf7796/d41586-020-00530-6.pdf, with pages: [489, 490]
# Found page: 490, looking for page: 491, index: 1
# Compiling file: pdf7796/d41586-020-00530-6.pdf, indices: [1], pages: [490]
# Found the last page of the previous article in the current article
# Dropping page 490 from file pdf7796/d41586-020-00530-6.pdf
# Page: 490, Checking pdf7796/d41586-020-00531-5.pdf, with pages: [490]
# Found page: 490, looking for page: 491, index: 0
# Compiling file: pdf7796/d41586-020-00531-5.pdf, indices: [0], pages: [490]
# Page: 491, Checking pdf7796/d41586-020-00505-7.pdf, with pages: [491]
# Found page: 491, looking for page: 492, index: 0

    def test_indices_page1(self):
        # Setup
        file_indices_dictionary, file_pages_dictionary, expected_page1, expected_page2 = self.get_expected_data()
        actual_indices1 = file_indices_dictionary[expected_page1['file']]

        # Test
        self.assertEqual(actual_indices1, expected_page1['indices'])

    def test_pages_page1(self):
        # Setup
        file_indices_dictionary, file_pages_dictionary, expected_page1, expected_page2 = self.get_expected_data()

        actual_pages1 = file_pages_dictionary[expected_page1['file']]

        # Test
        self.assertEqual(actual_pages1, expected_page1['pages'])

    def test_indices_page2(self):
        # Setup
        file_indices_dictionary, file_pages_dictionary, expected_page1, expected_page2 = self.get_expected_data()
        actual_indices2 = file_indices_dictionary[expected_page2['file']]

        # Test
        self.assertEqual(actual_indices2, expected_page2['indices'])

    def test_pages_page2(self):
        # Setup
        file_indices_dictionary, file_pages_dictionary, expected_page1, expected_page2 = self.get_expected_data()

        actual_indices2 = file_indices_dictionary[expected_page2['file']]
        actual_pages2 = file_pages_dictionary[expected_page2['file']]

        # Test
        self.assertEqual(actual_pages2, expected_page2['pages'])


if __name__ == '__main__':
    unittest.main()
