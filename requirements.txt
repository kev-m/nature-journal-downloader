requests==2.18.4
beautifulsoup4==4.6.0
urllib3==1.22
PyPDF4==1.27.0
fpdf==1.7.2
autopep8==1.5.2
pycodestyle==2.6.0

