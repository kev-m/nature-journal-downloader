# Contributing
To contribute:
1. Clone this repository to your own GitLab/GitHub repo.
2. In your repo:
    1. Create a branch with your contribution.
    2. Commit your changes to your branch.
    3. Reformat your code, as specified below
3. Submit a merge request back to this repository.

# Code Format

Before submitting any changes, apply the following code formatting command:
```bash
python3 -m autopep8 -i --max-line-length 110 --aggressive nature_journal_dl.py nature_dl/*.py tests/*.py
```
## Installing autopep8
[autopep8](https://pypi.org/project/autopep8/) can be installed with pip
```bash
pip3 install autopep8
```

# Ideas
Check the [Issues](https://gitlab.com/kev-m/nature-journal-downloader/-/issues)
for ideas on where help is needed.
