# Extract the Nature issue page numbers from the PDF for an individual article: Given a database and a
# list of PDFs, extract the page numbers of all the content.
# The problem is that a single page in Nature can contain content from multiple articles. Therefore,
# the individual article PDFs can contain duplicate pages.
# The purpose here is to build a dictionary of "pages" per "PDF file" so that a final output of unique,
# sequential pages can be created.
#
# e.g. for the following files containing the given pages, the compiled PDF should consist of only F2,
# F4 and pages 2 & 3 from F5
#  File: Pages
#  F1: 8
#  F2: 8,9
#  F3: 9,10
#  F4: 10,11,12
#  F5: 12,13,14

import logging
import sys

# Parsing HTML
from bs4 import BeautifulSoup

# Need to specifiy the CODEC for Windows / codepage
import codecs

from collections import OrderedDict
from PyPDF4 import PdfFileReader

# Extract the article id from the article <a href..>
# e.g. "d41586-019-02491-x" from "/articles/d41586-019-02491-x"


def add_to_articles(articles, entry):
    href = entry.get('href')
    leader = '/articles/'
    if href.find(leader) > -1:
        key = href[len(leader):]
        logging.debug("key=%s", key)
        articles.append(key)

# Find the article <a href..> entries in the issue HTML using the following patterns
# In the master issue HTML:
#    <a href="/articles/d41586-020-01264-1"
#                     itemprop="url"
#                     data-track="click"
#                     data-track-action="view article"
#                     data-track-label="link"
#    >Show evidence that apps for COVID-19 contact-tracing are secure and effective</a>
#
# In the article HTML:
#    <a href="//media.nature.com/original/magazine-assets/d41586-020-01264-1/d41586-020-01264-1.pdf"
#       target="_blank"
#       rel="noreferrer noopener"
#       data-track="download"
#       data-track-label="PDF download">PDF version</a>


def is_article(tag):
    if (tag.has_attr('data-track-action') and tag['data-track-action'] == 'view article'):
        href = tag.get('href')
        # Add the 'd' since only URLs that match '/articles/d*' are downloadable.
        if href.find('/articles/d') > -1:
            return True
    return False

# Parse the issue HTML for all "article" <a href..>s and extract article identifiers


def extract_issue_articles(html_path):
    articles = []
    # Specify the encoding
    with codecs.open(html_path, "r", "UTF-8") as fp:
        soup = BeautifulSoup(fp, "html.parser")

        for each in soup.find_all(is_article):
            add_to_articles(articles, each)

    return articles

# Given: The list of pdf_files for this issue
# Return: the dictionary of unique pages, keyed by input filename


def extract_issue_info_from_pdfs(pdf_files):
    # PDF/Page dictionary
    file_pages_dictionary = OrderedDict()

    last_page = 0
    # Given the list of articles, get the PDF information
    for pdf_file in pdf_files:
        logging.debug("Processing PDF %s", pdf_file)
        # Define empty page array
        page_array = []
        # It can happen that first_page is 0 when the PDF has no /PageLabel object
        first_page, number_of_pages = get_pdf_page_info(pdf_file, last_page)
        last_page = first_page + number_of_pages - 1
        for page in range(number_of_pages):
            page_array.append(first_page + page)
        # Add the array to the dictionary
        file_pages_dictionary[pdf_file] = page_array

    return file_pages_dictionary

# Algorithm: Sequentially extract the pages as you find them
# Given a page-sorted dictionary that contains {"filename", [page numbers]}, extract the sequential pages
# as you find them.
#   e.g. {filename1: [pg0, pg1, pg2], filename2:[pg2, pg3], filename3:[pg3, pg4], ...}
# Return: dictionary of unique page indices and dictionary of unique pages, keyed by input filename,
# e.g. {filename1: [0, 1], filename2:[0], filename3:[0], ...}, {filename1:
# [pg0, pg1], filename2:[pg2], filename3:[pg3], ...}


def extract_sequential_pages(sorted_dictionary):
    files = [*sorted_dictionary.keys()]

    file_indices_dictionary = OrderedDict()
    file_pages_dictionary = OrderedDict()

    current_index = 0
    current_page = 0
    last_page = 0

    while current_index < len(files):
        # "corrections" appear at the end of an article, with the same page number as the next article.
        # The duplicate "corrections" page overwrites the desired start page of the next article.
        # Solution: Drop the last page of the current article if it appears anywhere in the next article
        this_pdf = files[current_index]
        this_pdf_pages = sorted_dictionary[this_pdf]

        if this_pdf_pages.count(last_page) > 0:
            logging.debug("Found the last page of the previous article in the current article")
            # Drop the last page from the previous article.
            previous_pdf = files[current_index - 1]
            previous_pdf_pages = file_pages_dictionary[previous_pdf]
            previous_pdf_indices = file_indices_dictionary[previous_pdf]

            dropped_page = previous_pdf_pages.pop()
            dropped_page_index = previous_pdf_indices.pop()
            logging.debug("Dropping page %s from file %s", dropped_page, previous_pdf)

            current_page = dropped_page

        file_index, next_pages_indices, next_pages, last_page, current_page = get_pages_within_pdf_entry(
            sorted_dictionary, files, current_index, current_page)

        if len(next_pages_indices) > 0:
            logging.debug("Compiling file: %s, indices: %s, pages: %s",
                          files[file_index], next_pages_indices, next_pages)
            file_indices_dictionary[files[file_index]] = next_pages_indices
            file_pages_dictionary[files[file_index]] = next_pages

        current_index = file_index + 1

    logging.debug("No more files")

    return file_indices_dictionary, file_pages_dictionary

# Return the page number from the PDF file data


def extract_first_page_number_from_pdf(page):
    obj = page.getObject()
    page_number = obj['/St']
    return page_number


# Given a dictionary of: {'file1': [..pages..], 'file2': [..pages..],  ...}, e.g.
#  {'file-1.pdf': [149], 'file-2.pdf': [150], 'file-3.pdf': [151], 'file-4.pdf': [283, 284, 285],
#   'file-5.pdf': [163, 164, 165, 166]}
# Return a sorted dictionary where the pages are in increasing order, e.g.:
#  {'file-1.pdf': [149], 'file-2.pdf': [150], 'file-3.pdf': [151], 'file-5.pdf': [163, 164, 165, 166],
#   'file-4.pdf': [283, 284, 285]}
def sort_page_dictionary(unsorted_dictionary):
    logging.debug("Un-sorted: %s", unsorted_dictionary)
    sorted_dictionary = OrderedDict(sorted(unsorted_dictionary.items(), key=lambda t: t[1]))
    logging.debug("Sorted: %s", sorted_dictionary)

    return sorted_dictionary

# Determine the page number of the first page of the PDF and number of pages in the PDF
# Parameters:
#  path to PDF
#  page number of the last page found
# Returns: first page number of this PDF, number of pages
#
# Sometimes an article PDF does not have actual page numbers in its index.
# Solution: Guess the first_page by using the previous 'last_page'
# As last_page+number_of_pages


def get_pdf_page_info(path, last_page):
    with open(path, 'rb') as f:
        pdf = PdfFileReader(f)
        info = pdf.getDocumentInfo()
        number_of_pages = pdf.getNumPages()

        logging.debug("Number of pages = %d", number_of_pages)

        try:
            page_label_types = pdf.trailer["/Root"]["/PageLabels"]["/Nums"]
            first_page = extract_first_page_number_from_pdf(page_label_types[1])
            logging.debug("First page is number %d", first_page)
            return first_page, number_of_pages
        except BaseException:
            logging.warning("No /PageLabel object for %s, making a guess for the page number", path)
            return last_page + number_of_pages, number_of_pages

# Determine the current file index and the list of pages it provides
# - dictionary consists of {"filename", [page numbers]}
# - files consists of an array of PDF filenames
# - current_index is the index of the current filename in the "files" array
# - current_page is the current page that has been found, maybe be 0 (unknown)
# Return:
#  - file_index, the index of the files entry tried
#  - pages, the array of pages that this file provides
#  - next_page, the next page number to search for


def get_pages_within_pdf_entry(dictionary, files, current_index, current_page):
    this_pages_index = []  # Index of next unique page
    this_pages = []  # Page number of next unique page
    next_page = current_page

    # Search for the next page (at least current_page+1, but maybe higher)
    next_pdf = files[current_index]
    next_pdf_pages = dictionary[next_pdf]

    logging.debug("Page: %s, Checking %s, with pages: %s", current_page, next_pdf, next_pdf_pages)

    page_index = 0
    last_page = 0
    for page in next_pdf_pages:
        last_page = page
        if page >= next_page:
            this_pages_index.append(page_index)
            this_pages.append(page)
            next_page = page + 1
            logging.debug("Found page: %s, looking for page: %s, index: %s", page, next_page, page_index)

        page_index += 1

    return current_index, this_pages_index, this_pages, last_page, next_page

# Given the {file: [page_numbers]} data, find the groups of sequential pages
# suitable for the PDF PageLabels metadata.
# Return a dictionary of {index:page_start}, for all page number groups


def find_pagelabel_sequences(file_pages_dictionary):
    pagelabel_indices = {}
    index = 1  # First PageLabel index is the 2nd page (index 1)
    current_page = 0  # Initialise the first page to 0

    for pdf_file in file_pages_dictionary.keys():
        pages = file_pages_dictionary[pdf_file]
        for next_page in pages:
            # If the next page is simply the next in sequence, ignore it
            if current_page + 1 != next_page:  # 564
                # Store the next_page
                pagelabel_indices[index] = next_page
                logging.debug("Page group found at index %s for page %s", index, next_page)
            # compute next page and increment index
            current_page = next_page
            index = index + 1

    return pagelabel_indices
