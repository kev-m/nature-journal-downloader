# Download the issue HTML and the article PDFs from Nature.com

# Basic stuff
import os
import sys
# Logging
import logging

# URL downloading, etc.
import requests

# Fetching issue HTML and PDFs from web servers
import urllib.request

# CODEC needed for Windows
import codecs

# To Sort pages
from collections import OrderedDict

# Check if a file exists.
# Removes the file if the file exists and cached is False
# Return True only if the file exists and cached is True, else
# Return False


def _use_cached_file_if_exists(file_name, cached=True):
    if os.path.isfile(file_name) is True:
        if cached is True:
            logging.debug("Cached is True, using file_name %s", file_name)
            return True
        logging.debug("Cached == False, Deleting file_name %s", file_name)
        os.remove(file_name)
    return False

# Download the issue from:
#   https://www.nature.com/nature/volumes/${volume}/issues/${issue}


def download_issue_html_by_volume_and_issue(volume, issue, cached=True, temp_dir='temp'):
    issue_html_file = temp_dir + "/issue" + issue + ".html"

    if _use_cached_file_if_exists(issue_html_file, cached) is True:
        return issue_html_file

    url = "https://www.nature.com/nature/volumes/" + volume + "/issues/" + issue
    logging.info("Downloading content from URL: %s", url)
    r = requests.get(url, allow_redirects=True)

    logging.debug("Content encoding is: %s", r.encoding)
    logging.debug("Saving to issue_html_file %s", issue_html_file)
    try:
        # Use the appropriate CODEC
        with codecs.open(issue_html_file, "w", "UTF-8") as temp:
            temp.write(r.text)
    except IOError as e:
        logging.error("Error writing content to file", e)
        return None

    return issue_html_file

# Download the issue cover from:
#   https://media.springernature.com/w200/springer-static/cover-hires/journal/41586/${volume}/${issue}
# Return: dictionary with keys 'issue_cover_file' and 'image_type'


def download_issue_cover_by_volume_and_issue(volume, issue, cached=True, temp_dir='temp'):
    issue_cover_file = temp_dir + "/issue" + issue + "_cover.png"

    if _use_cached_file_if_exists(issue_cover_file, cached) is True:
        # Risky, use defaults.
        cover_image_dict = {'issue_cover_file': issue_cover_file, 'image_type': 'png'}
        return cover_image_dict

    # Either parse the HTML for the image link or just use this magic URL that works:
    url = "https://media.springernature.com/w200/springer-static/cover-hires/journal/41586/" + volume + "/" + issue
    logging.info("Downloading cover from URL: %s", url)

    local_filename, result = urllib.request.urlretrieve(url, filename=issue_cover_file)
    logging.debug("urlretrieve as_string() is: %s", result.as_string())
    content_type = result.get("Content-Type", "Unknown")  # image/png
    logging.debug("Result content_type is : %s", content_type)

    if content_type == "image/png":
        image_type = 'png'
    else:
        logging.warning("Expected PNG content, received %s", content_type)

    logging.debug("Downloaded cover image to: %s", local_filename)

    cover_image_dict = {'issue_cover_file': issue_cover_file, 'image_type': image_type}

    return cover_image_dict


# Given an input URL, find the final destination URL (Nature.com includes
# authorisation and other redirects)
def get_final_url(url):
    # Nature requires dummy authorisation, so enable redirects to let Python sort it all out
    header = requests.head(url, allow_redirects=True)
    url = header.url

    # e.g. url=https://www.nature.com/nature/volumes/578/issues/7795
    logging.debug("url=%s", url)

    return url

# Given: A base URL, an output directory and a list of article IDs,
# download the PDFs of the articles from the URL, assuming:
# The origin URL is base_url+<article>/<article>.pdf, e.g.:
# https://www.nature.com/magazine-assets/<article>/<article>.pdf
# Return: List of PDF filenames (full path)


def fetch_article_pdfs(base_url, pdf_directory, articles):
    pdf_files = []
    failures = []

    for article in articles:
        filename = article + '.pdf'
        pdf_file = pdf_directory + '/' + filename
        try:
            if os.path.isfile(pdf_file) is False:
                logging.info("Downloading %s", pdf_file)
                URL = base_url + article + '/' + filename
                result = urllib.request.urlretrieve(URL, filename=pdf_file)

            pdf_files.append(pdf_file)
        except Exception as e:
            failures.append(pdf_file)
            logging.warning("Unable to download file: %s", pdf_file)
            logging.warning(e)
            continue

    return pdf_files, failures
