# Use PyPDF to manipulate pages
# https://pythonhosted.org/PyPDF2/  https://github.com/claird/PyPDF4
from PyPDF4 import PdfFileWriter, PdfFileReader

# To manipulate the PDF dictionary
import PyPDF4.pdf as PDF

# Use PyFPDF to create cover page with image
from fpdf import FPDF

# Debugging
import inspect
import pprint

# Create a PDF file from the provided cover image
# Parameters:
#   cover_image_dict dictionary with keys 'issue_cover_file' and 'image_type'
#   temp_pdf_directory where to create the PDF
# Return the path to the generated PDF file


def generate_issue_cover_pdf(volume, issue, cover_image_dict, temp_pdf_directory):
    issue_cover_pdf = temp_pdf_directory + "/issue" + issue + "_cover.pdf"

    pdf = FPDF('P', 'mm', 'A4')
    pdf.add_page()
    pdf.image(name=cover_image_dict['issue_cover_file'],
              x=None, y=None,
              w=190, h=0,
              type=cover_image_dict['image_type'],
              link='')
    pdf.output(issue_cover_pdf, 'F')
    return issue_cover_pdf

# From Example 8.3 in the PDF Specification
# /PageLabels << /Nums [ 0 << /S /r >>
#                       4 << /S /D >>
#                       7 << /S /D
#                            /P (A−)
#                            /St 8
#                         >>
#                    ]
#        >>
# Pages 0 to 3: Lowercase roman numerals (i,ii, ....)
# Pages 4 to 6: Decimal Arabic numbers (1, 2, 3, ...)
# Pages 7 onwards: Decimal with ASCII prefix "A-" (A-1, A-2, A-3)


def pdf_pagelabels_roman():
    number_type = PDF.DictionaryObject()
    number_type.update({PDF.NameObject("/S"): PDF.NameObject("/r")})
    return number_type


def pdf_pagelabels_decimal():
    number_type = PDF.DictionaryObject()
    number_type.update({PDF.NameObject("/S"): PDF.NameObject("/D")})
    return number_type


def pdf_pagelabels_decimal_with_offset(offset):
    number_type = pdf_pagelabels_decimal()
    number_type.update({PDF.NameObject("/St"): PDF.NumberObject(offset)})
    return number_type


def create_pagelabels(pdf_writer, pagelabel_dictionary):
    # We want the opposite of (see 8.3.1 in PDF spec):
    # page_label_types = pdf.trailer["/Root"]["/PageLabels"]["/Nums"]
    # As per: https://github.com/psammetichus/PDF-Add-Outline/blob/master/addindex.py
    nums_array = PDF.ArrayObject()
    nums_array.append(PDF.NumberObject(0))  # Page 0:
    nums_array.append(pdf_pagelabels_roman())  # Roman numerals

    # TODO: If the Table of contents is created, the offset must be handled here

    # Create the "/Nums" entries
    for pagelabel_index in pagelabel_dictionary.keys():
        page_label_value = pagelabel_dictionary[pagelabel_index]
        nums_array.append(PDF.NumberObject(pagelabel_index))
        nums_array.append(pdf_pagelabels_decimal_with_offset(
            page_label_value))  # Decimal numbers, with Offset

    page_numbers = PDF.DictionaryObject()
    page_numbers.update({PDF.NameObject("/Nums"): nums_array})

    page_labels = PDF.DictionaryObject()
    page_labels.update({PDF.NameObject("/PageLabels"): page_numbers})

    return page_labels

# Given an output filename and a dictionary, generate the concatenated output from the dictionary.
# Where the dictionary consists of "filename -> list of page indices" for each input PDF file, e.g.:
#   {"file1":[0], "file2":[0,1], "file3":[1], ...}
# then the output conists of page 1 from file1, pages 1 & 2 from file2, page 2 from file3, ...


def generate_single_issue_pdf(volume, issue, pdf_output_name, file_page_dictionary, pagelabel_dictionary):
    output = PdfFileWriter()

    # TODO: Add table of contents (Issue 2: https://gitlab.com/kev-m/nature-journal-downloader/-/issues/2)

    # Iterate over all pdf_files in dictionary of unique pages
    pdf_files = [*file_page_dictionary.keys()]
    for pdf_file in pdf_files:
        input_pdf = PdfFileReader(open(pdf_file, 'rb'))
        page_indices = file_page_dictionary[pdf_file]
        for page_index in page_indices:
            page = input_pdf.getPage(page_index)
            output.addPage(page)

    # Add PageLabels
    page_labels = create_pagelabels(output, pagelabel_dictionary)
    root_obj = output._root_object
    root_obj.update(page_labels)

    # Add metadata
    output.addMetadata({
        '/Title': 'Nature, Volume ' + volume + ', Issue ' + issue,
        '/Subject': 'Science',
        '/Author': 'Springer Nature Limited',
        '/Keywords': 'Science, Journal',
        '/Creator': 'https://gitlab.com/kev-m/nature-journal-downloader',
    })

    with open(pdf_output_name, 'wb') as f:
        output.write(f)
